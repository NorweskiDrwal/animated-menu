export type DesktopMenuItem = {
    url: string,
    name: string,
};

export type MobileMenuItem = {
    url: string,
    name: string,
    icon: string,
};

export const desktopMainMenuList = [
    { name: 'Home',         url: '/'        },
    { name: 'Flights',      url: '#'        },
    { name: 'About Us',     url: '/about'   },
    { name: 'FAQ',          url: '/awaymo'  },
    { name: 'Support',      url: '/awaymo'  },
    { name: 'Contact Us',   url: '/contacts'},
];

export const desktopUserMenuList = [
    { name: 'Profile',              url: '/profile'         },
    { name: 'My Bookings',          url: '/user/bookings'   },
    { name: 'My Payments',          url: '/user/payments'   },
    { name: 'Log Out',              url: '/'                },
    { name: 'Resume Application',   url: '#'                },
];

export const mobileMenuList = [
    { name: 'Profile',      url: '/profile',        icon: 'user-circle'     },
    { name: 'My Bookings',  url: '/user/bookings',  icon: 'plane'           },
    { name: 'My Payments',  url: '/user/payments',  icon: 'credit-card'     },
    { name: 'Support',      url: '/awaymo',         icon: 'life-ring'       },
    { name: 'Contact Us',   url: '/contacts',       icon: 'phone'           },
    { name: 'Log Out',      url: '/',               icon: 'sign-out-alt'    },
    { name: 'About',        url: '/about',          icon: 'question-circle' },
    { name: 'FAQ',          url: '/awaymo',         icon: 'info-circle'     },
];
