import * as React from 'react';
import { Switch, Route } from 'react-router-dom';


const Router = () => (
    <Switch>
        <Route path="/profile" render={() => <p>[PROFILE PAGE]</p>} />
        <Route path="/user/bookings" render={() => <p>[BOOKINGS PAGE]</p>} />
        <Route path="/user/payments" render={() => <p>[PAYMENTS PAGE]</p>} />
        <Route path="/user/referrals" render={() => <p>[REFERRALS PAGE]</p>} />
        <Route path="/about" render={() => <p>[ABOUT US PAGE]</p>} />
        <Route path="/awaymo" render={() => <p>[SUPPORT PAGE]</p>} />
        <Route path="/contacts" render={() => <p>[CONTACT US PAGE]</p>} />
    </Switch>
);

export default Router;
