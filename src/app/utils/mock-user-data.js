// @flow

export type User = {
    title: string,
    firstName: string,
    lastName: string,
    email: string,
    balance: number,
}

export const user = {
    title: 'mr',
    firstName: 'Hulk',
    lastName: 'Smash',
    email: 'hulk@smash.you',
    balance: 1234.56,
};
