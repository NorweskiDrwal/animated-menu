// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Transition  } from 'react-transition-group';


type Props = {
    isMenuOpen: boolean,
    children: React.Node,
};

const SlideY = (props: Props) => (
    <Transition in={props.isMenuOpen} timeout={300}>
        {state => (
            <StyledWrapper style={{ ...transitionStyles[state] }}>
                {props.children}
            </StyledWrapper>
        )}
    </Transition>
);

const transitionStyles = {
    entering: { transform: 'translateY(0)' },
    entered: { transform: 'translateY(0)' },
    exiting: { transform: 'translateY(-100vh)' },
    exited: { transform: 'translateY(-100vh)' },
};

const StyledWrapper = styled.div`
    transform: translateY(-100vh);
    transition: transform .3s ease-in-out;
`;

export default SlideY;
