// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Color } from 'app/assets/styles';
import { Container, Row, Col } from 'react-grid-system';

import { type User, Viewport } from 'app/utils';

import SlideY from 'app/utils/SlideY';
import MainMenu from './MainMenu';
import UserMenu from './UserMenu';
import MenuHeader from './MenuHeader';
import MenuFooter from './MenuFooter';


type Props = {
    userData: User,
    isMenuOpen: boolean,
    handleSetMenuClose(): void,
};

const DesktopMenu = (props: Props) => (
    <SlideY isMenuOpen={props.isMenuOpen}>
        <StyledMenuContainer>
            <StyledMenuWrapper>
                <MenuHeader handleSetMenuClose={props.handleSetMenuClose} />

                <Container fluid style={styledContainer}>
                    <Row nogutter>
                        <Col md={4}>
                            <MainMenu />
                        </Col>
                        <Col md={5}>
                            <UserMenu userData={props.userData} />
                        </Col>
                    </Row>
                </Container>

                <MenuFooter />
            </StyledMenuWrapper>
        </StyledMenuContainer>
    </SlideY>
);

const styledContainer = {
    paddingLeft: 0,
    paddingRight: 0,
};
const StyledMenuContainer = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    color: #fff;
    height: 100vh;
    padding: 0 50px;
    font-size: 34px;
    overflow-y: auto;
    position: absolute;
    background: ${Color.main};
    
    @media (max-width: ${Viewport.TABLET}px) {
        font-size: 24px;
    }
    
    @media (max-width: 850px) {
        font-size: 30px;
    }
`;
const StyledMenuWrapper = styled.section`
    width: 100%;
    display: block;
    max-width: 1236px;
    margin: 0 auto;
    height: fit-content;
`;

export default DesktopMenu;
