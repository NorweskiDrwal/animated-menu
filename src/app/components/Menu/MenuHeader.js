// @flow

import * as React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';

import logo from 'app/assets/awaymoFullWhite.svg';
import { Viewport } from 'app/utils/resize-listener';


type Props = {
    history: { push(): void },
    handleSetMenuClose(): void,
};

const MenuHeader = (props: Props) => {
    const toHome = (): void => props.history.push('/');

    return (
        <StyledMenuHeader>
            <StyledLogo
                src={logo}
                onClick={toHome}
                alt="Awaymo – Leading Provider Of Pay Monthly Holidays"
            />
            <StyledCloseIcon icon="times" onClick={props.handleSetMenuClose} />
        </StyledMenuHeader>
    );
};

const StyledMenuHeader = styled.header`
    height: 100px;
    position: relative;
    margin-bottom: 30px;
    border-bottom: 3px solid #fff;
    
    @media (max-width: ${Viewport.MOBILE}px) {
        height: 70px;
        border-bottom: 2px solid rgba(255,255,255,0.4);
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        margin-bottom: 15px;
    }
`;
const StyledLogo = styled.img`
    left: 0;
    top: 50%;
    float: left;
    height: 35px;
    cursor: pointer;
    position: absolute;
    transform: translateY(-50%);
    
    @media (max-width: ${Viewport.MOBILE}px) {
        left: 50%;
        height: 30px;
        transform: translate(-50%, -50%);
    }
`;
const StyledCloseIcon = styled(Icon)` && {
    top: 50%;
    right: 0;
    cursor: pointer;
    font-size: 32px;
    position: absolute;
    transform: translateY(-50%);
    
    @media (max-width: ${Viewport.MOBILE}px) {
        font-size: 22px;
    }
}`;

export default withRouter(MenuHeader);
