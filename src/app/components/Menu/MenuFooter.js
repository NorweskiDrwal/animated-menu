// @flow

import * as React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { Viewport } from 'app/utils/resize-listener';


const MenuFooter = () => {
    const contactSupport = (): void => {
        const email = 'mailto:support@awaymo.com?Subject=I need help!';
        window.open(email);
    };

    return (
        <StyledFooterWrapper>
            <StyledMenuFooter>
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                <p>We're here to help</p>
                <p>+44 (0) 20 8050 3459</p>
                <StyledSupport>
                    {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions */}
                    <span onClick={contactSupport}>
                        support<StyledAtIcon icon="at" />awaymo.com
                    </span>
                </StyledSupport>
            </StyledMenuFooter>
        </StyledFooterWrapper>
    );
};

const StyledFooterWrapper = styled.footer`
    width: 100%;
    display: block;
    margin: 30px auto;
    max-width: 1236px;
    height: fit-content;
    
    @media (max-height: 736px) {
        display: none;
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        margin-top: 15px;
    }
    
    @media (max-width: ${Viewport.TABLET}px) and (max-height: 800px) {
        display: none;
    }
    
    @media (max-width: ${Viewport.MOBILE}px) {
        margin-top: 30px;
    }
`;
const StyledMenuFooter = styled.div`
    font-size: 20px;
    padding-top: 30px;
    text-align: center;
    border-top: 2px solid rgba(255,255,255,0.3);
    p {
        margin: 0;
        padding: 0;
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        padding-top: 0;
        border-top-width: 0;
    }
    
    @media (max-width: ${Viewport.MOBILE}px) {
        padding-top: 15px;
        border-top-width: 1px;
    }
`;
const StyledAtIcon = styled(Icon)` && {
    font-size: 14px;
    
    @media (max-width: ${Viewport.TABLET}px) {
        font-size: 10px;
    }
}`;
const StyledSupport = styled.p`
    span {
        cursor: pointer;
        width: fit-content;
    }
`;

export default MenuFooter;
