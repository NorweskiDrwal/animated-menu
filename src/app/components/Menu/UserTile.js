// @flow

import * as React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import { Color } from 'app/assets/styles';
import placeholder from 'app/assets/user.png';
import {
    type User, useWindowWidth, formatter, Viewport,
} from 'app/utils';


type Props = {
    userData: User,
    history: { push(): void },
}

const UserTile = (props: Props) => {
    const isMobile: boolean = useWindowWidth() < Viewport.MOBILE;
    const openUserProfile = (): void => props.history.push('/profile');

    return (
        <StyledUserTile>
            <StyledUserAvatarWrapper onClick={openUserProfile}>
                <StyledUserAvatar alt="user avatar" src={placeholder} />
            </StyledUserAvatarWrapper>

            <StyledUserDetails>
                <div>{props.userData.firstName}</div>
                {isMobile && <div>Available Balance</div>}
                <div>
                    {formatter.format(props.userData.balance)}
                    {!isMobile && ' Available'}
                </div>
            </StyledUserDetails>
        </StyledUserTile>
    );
};

const StyledUserTile = styled.div`    
    @media (min-width: ${Viewport.MOBILE}px) {
        margin-bottom: 30px;
        padding-bottom: 30px; 
        border-bottom: 1px solid rgba(255,255,255,0.4);
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        margin-top: 15px;
        text-align: center;
        margin-bottom: 15px;
        padding-bottom: 15px; 
    }
    
    @media (max-width: ${Viewport.MOBILE}px) {
        margin-top: 15px;
        margin-bottom: 0;
        padding-bottom: 0; 
        text-align: center;
    }
`;
const StyledUserAvatarWrapper = styled.div`
    width: 60px;
    height: 60px;
    display: block;
    background: #fff;
    position: relative;
    border-radius: 50%;
    margin: 0 auto;
    
    @media (min-width: ${Viewport.MOBILE}px) {
        width: 76px;
        height: 76px;
        cursor: pointer;
        display: inline-block;
        vertical-align: middle;
        
        :after {
            top: 50%;
            left: 50%;
            width: 64px;
            content: "";
            height: 64px;
            position: absolute;
            border-radius: 50%;
            border: 2px solid ${Color.main};
            transform: translate(-50%,-50%);
        }
    }
`;
const StyledUserAvatar = styled.img`
    top: 50%;
    left: 50%;
    height: 40px;
    position: absolute;
    transform: translate(-50%,-50%);
`;
const StyledUserDetails = styled.div`
    font-size: 24px;
    font-weight: 500;
    display: inline-block;
    vertical-align: middle;
    div {
        margin-bottom: 4px;
    }
    div:nth-child(2) {
        font-size: 20px;
    }
    
    @media (min-width: ${Viewport.MOBILE}px) {
        font-weight: 900;
        margin-left: 21px;
        div:first-child {
            text-align: left;
        }
    }
`;

export default withRouter(UserTile);
