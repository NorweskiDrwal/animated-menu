// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Link, withRouter } from 'react-router-dom';

import {
    type User, type DesktopMenuItem, desktopUserMenuList, Viewport,
} from 'app/utils';

import UserTile from './UserTile';


type Props = {
    userData: User,
    history: { push(): void },
};

const UserMenu = (props: Props) => {
    const printMenuItems = desktopUserMenuList.map((item: DesktopMenuItem, i: number): React.Node => (
        <StyledMenuItem key={i}>
            <Link to={item.url}>{item.name}</Link>
        </StyledMenuItem>
    ));

    return (
        <StyledUserMenuContainer>
            <UserTile userData={props.userData} />

            <StyledMenuList>
                {printMenuItems}
            </StyledMenuList>
        </StyledUserMenuContainer>
    );
};

const StyledUserMenuContainer = styled.nav`
    @media (max-width: ${Viewport.TABLET}px) {
        border-radius: 8px;
        border: 1px solid rgba(255,255,255,0.4);
    }
`;
const StyledMenuList = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    font-weight: 900;
    
    @media (max-width: ${Viewport.TABLET}px) {
        padding-left: 20px;
        margin-bottom: 15px;
        font-weight: 700;
    }
`;
const StyledMenuItem = styled.li`
    margin: 0;
    padding: 0;
    height: 50px;
    
    a {
        color: #fff;
        border-bottom: 3px solid transparent;
        
        :hover {
            border-bottom-color: #fff;
        }
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        height: 34px;
        
        a {
            border-width: 2px;
        }
    }
`;


export default withRouter(UserMenu);
