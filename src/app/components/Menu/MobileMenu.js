// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Link, withRouter } from 'react-router-dom';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome/index';

import { Color } from 'app/assets/styles';
import {
    type User, type MobileMenuItem, mobileMenuList, Viewport,
} from 'app/utils';

import SlideX from 'app/utils/SlideX';
import MenuHeader from './MenuHeader';
import MenuFooter from './MenuFooter';
import UserMenuTile from './UserTile';


type Props = {
    userData: User,
    isMenuOpen: boolean,
    history: { push(): void },
    handleSetMenuClose(): void,
};

const MobileMenu = (props: Props) => {
    const printMenuItems = mobileMenuList.map((item: MobileMenuItem, i: number): React.Node => {
        const isLast: boolean = i === (mobileMenuList.length - 1);

        return (
            <StyledLink to={item.url} key={i}>
                <StyledMenuItem last={isLast}>
                    <StyledMenuIcon icon={item.icon} />
                    <span>{item.name}</span>
                </StyledMenuItem>
            </StyledLink>
        );
    });

    return (
        <SlideX isMenuOpen={props.isMenuOpen}>
            <StyledMenuContainer>
                <StyledMenuWrapper>

                    <MenuHeader handleSetMenuClose={props.handleSetMenuClose} />

                    <UserMenuTile userData={props.userData} />

                    <StyledMenuList>
                        {printMenuItems}
                    </StyledMenuList>

                    <MenuFooter />

                </StyledMenuWrapper>
            </StyledMenuContainer>
        </SlideX>
    );
};

const StyledMenuContainer = styled.div`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    color: #fff;
    height: 100vh;
    padding: 0 50px;
    font-size: 24px;
    overflow-y: auto;
    position: absolute;
    background: ${Color.main};
    
    @media (max-width: 850px) {
        font-size: 30px;
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        font-size: 24px;
        padding: 0 20px;
    }
`;
const StyledMenuWrapper = styled.section`
    width: 100%;
    display: block;
    max-width: 1236px;
    height: fit-content;
    margin: 0 auto 15px;
`;
const StyledLink = styled(Link)` && {
    color: #fff;
}`;
const StyledMenuList = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
`;
const StyledMenuItem = styled.li`
    margin: 0;
    padding: 0;
    height: 50px;
    display: block;
    font-size: 20px;
    line-height: 50px;
    border-bottom: ${props => (props.last ? 'none' : '1px solid rgba(255,255,255,0.4)')};
    a {
        color: #fff;
    }
`;
const StyledMenuIcon = styled(Icon)` && {
    margin-right: 10px;
}`;

export default withRouter(MobileMenu);
