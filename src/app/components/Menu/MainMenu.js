// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { type DesktopMenuItem, desktopMainMenuList, Viewport } from 'app/utils';


const MainMenu = () => {
    const printMenuItems = desktopMainMenuList.map((item: DesktopMenuItem, i: number): React.Node => (
        <StyledMenuItem key={i}>
            <Link to={item.url}>{item.name}</Link>
        </StyledMenuItem>
    ));

    return (
        <StyledMainMenuWrapper>
            <StyledMenuList>
                {printMenuItems}
            </StyledMenuList>
        </StyledMainMenuWrapper>
    );
};

const StyledMainMenuWrapper = styled.nav`
    margin-bottom: 20px;
`;
const StyledMenuList = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    font-weight: 900;
    li:nth-child(2) {
        margin-bottom: 24px;
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        font-weight: 700;
        li:nth-child(2) {
            margin-bottom: 12px;
        }
    }
`;
const StyledMenuItem = styled.li`
    margin: 0;
    padding: 0;
    height: 50px;
    a {
        color: #fff;
        border-bottom: 3px solid transparent;
        :hover {
            border-bottom-color: #fff;
        }
    }
    
    @media (max-width: ${Viewport.TABLET}px) {
        height: 34px;
        a {
            border-width: 2px;
        }
    }
`;

export default MainMenu;
