import * as React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { BrowserRouter } from 'react-router-dom';

import App from 'app/App';
import Header from './Header';
import MobileMenu from './Menu/MobileMenu';
import DesktopMenu from './Menu/DesktopMenu';
import LayoutContainer from './LayoutContainer';


describe('<LayoutContainer>', () => {
    test('should render self correctly', () => {
        const container = mount(
            <BrowserRouter>
                <App>
                    <LayoutContainer />
                </App>
            </BrowserRouter>,
        );
        expect(container).toHaveLength(1);
    });

    test('should render <Header> correctly', () => {
        const container = mount(
            <BrowserRouter>
                <LayoutContainer />
            </BrowserRouter>,
        );
        expect(container.find(Header).exists()).toBeTruthy();
    });

    test('should open menu on menu bar or menu icon click', () => {
        const container = mount(
            <BrowserRouter>
                <LayoutContainer />
            </BrowserRouter>,
        );
        let desktopMenuOpen = container.find(DesktopMenu);

        expect(desktopMenuOpen.props().isMenuOpen).toEqual(false);

        act(() => container.find(Header).props().handleSetMenuOpen());

        container.update();
        desktopMenuOpen = container.find(DesktopMenu);

        expect(desktopMenuOpen.props().isMenuOpen).toEqual(true);
    });

    test('should render <DesktopMenu> if width more than 576', () => {
        window.innerWidth = 1000;
        const container = mount(
            <BrowserRouter>
                <LayoutContainer />
            </BrowserRouter>,
        );

        expect(container.find(MobileMenu).exists()).toBeFalsy();
        expect(container.find(DesktopMenu).exists()).toBeTruthy();
    });

    test('should render <MobileMenu> if width less than 576', () => {
        window.innerWidth = 500;
        const container = mount(
            <BrowserRouter>
                <LayoutContainer />
            </BrowserRouter>,
        );

        expect(container.find(MobileMenu).exists()).toBeTruthy();
        expect(container.find(DesktopMenu).exists()).toBeFalsy();
    });
});
