// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Container, Row, Col } from 'react-grid-system';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';


type Props = {
    handleSetMenuOpen(): void,
}

const Header = (props: Props) => (
    <StyledHeader onClick={props.handleSetMenuOpen}>
        <Container fluid style={styledContainer}>
            <Row nogutter justify="end" align="center">
                <StyledCol>
                    <StyledMenuIcon icon="bars" onClick={props.handleSetMenuOpen} />
                </StyledCol>
            </Row>
        </Container>
    </StyledHeader>
);

const StyledHeader = styled.header`
    top: 0;
    left: 0;
    right: 0;
    padding: 20px;
    position: absolute;
    background: linear-gradient(
        rgba(0,0,0,.5),
        rgba(0,0,0,0)),url(https://www.awaymo.com/static/dist/imgs/introBackgroundSummer.jpg
    );
    background-size: cover;
`;
const styledContainer = {
    paddingLeft: 0,
    paddingRight: 0,
};
const StyledCol = styled(Col)` && {
    text-align: right;
}`;
const StyledMenuIcon = styled(Icon)` && {
    color: #fff;
    cursor: pointer;
    font-size: 32px;
}`;

export default Header;
