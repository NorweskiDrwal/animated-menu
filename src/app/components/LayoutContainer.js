// @flow

import * as React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { Header, DesktopMenu, MobileMenu } from 'app/components';

import Router from 'app/utils/Router';
import { useWindowWidth, user } from 'app/utils';


type Props = {
    location: { pathname: string },
}

const LayoutContainer = (props: Props) => {
    const [menuOpen, setMenuOpen] = React.useState(false);

    React.useEffect(() => {
        setMenuOpen(false);
    }, [props.location.pathname]);

    const handleSetMenuOpen = (): void => {
        setMenuOpen(true);
        document.body.style.overflowY = 'hidden';
    };
    const handleSetMenuClose = (): void => {
        setMenuOpen(false);
        document.body.style.overflowY = 'auto';
    };


    const renderMenuPerWidth = useWindowWidth() > 576
        ? <DesktopMenu handleSetMenuClose={handleSetMenuClose} isMenuOpen={menuOpen} userData={user} />
        : <MobileMenu handleSetMenuClose={handleSetMenuClose} isMenuOpen={menuOpen} userData={user} />;

    return (
        <StyledMain>
            <Header handleSetMenuOpen={handleSetMenuOpen} />
            <aside>
                {renderMenuPerWidth}
            </aside>
            <StyledSection>
                <Router />
            </StyledSection>
        </StyledMain>
    );
};

const StyledMain = styled.main`
    height: 100vh;
    position: relative;
    overflow-x: hidden;
`;

const StyledSection = styled.section`
    margin-top: 100px;
    text-align: center;
`;

export default withRouter(LayoutContainer);
