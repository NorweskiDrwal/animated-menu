import * as React from 'react';
import Layout from 'app/components/LayoutContainer';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faBars,
    faTimes,
    faAt,
    faUserCircle,
    faPlane,
    faCreditCard,
    faLifeRing,
    faPhone,
    faSignOutAlt,
    faQuestionCircle,
    faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';

import 'app/assets/global.css';

library.add(
    faBars,
    faTimes,
    faAt,
    faUserCircle,
    faPlane,
    faCreditCard,
    faLifeRing,
    faPhone,
    faSignOutAlt,
    faQuestionCircle,
    faInfoCircle,
);

export default () => <Layout />;
