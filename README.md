# animated-menu
I chose not to use redux for a project of this size.

I used Flow for basic type safety and also included react-router for basic routing and navigation.

I decided not to eject the project for absolute paths and went with `.env` solution.

I refrained from testing everything and focused solely on logic present in the `LayoutContainer` component.
